class StaticPagesController < ApplicationController
  def home
    if logged_in?
      @micropost  = current_user.microposts.build
      @feed_items = current_user.feed.paginate(page: params[:page]).search(params[:search])
      @comment = Comment.new
      @comments = @micropost.comments
    end
    # app/views/リソース名/リソース名/アクション名.リソース名/アクション名.html.erb
    # app/views/static_pages/home.html.erb
  end
  
  def terms
  end

end
