class PasswordEditsController < ApplicationController
  before_action :logged_in_user, only: [:edit, :update]
  before_action :correct_user, only: [:edit, :update]
  
  def edit
  end
  
  def update
    if !@user.authenticated?(:password, params[:user][:password_pressent])
      @user.errors.add(:base, 'Pressent password not match')
      render 'edit'
    elsif params[:user][:password].empty?
      @user.errors.add(:password, :blank)
      render 'edit'
    elsif @user.update_attributes(user_params)
      log_in @user
      flash[:success] = "Password has been edit."
      redirect_to @user
    else
      render 'edit'
    end
  end
  
  private
  
    def user_params
      params.require(:user).permit(:password, :password_confirmation)
    end
end
