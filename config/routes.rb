Rails.application.routes.draw do
  root 'static_pages#home'
  get  '/terms',      to: 'static_pages#terms'
  get  '/signup',     to: 'users#new'
  post  '/signup',    to: 'users#create'
  get    '/login',    to: 'sessions#new'
  post   '/login',    to: 'sessions#create'
  delete '/logout',   to: 'sessions#destroy'
  get 'auth/:provider/callback', to: 'sessions#create'
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :users
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :microposts,          only: [:show, :new, :create, :destroy]
  resources :relationships,       only: [:create, :destroy]
  resources :likes,               only: [:create, :destroy]
  resources :microposts do
    resources :comments,          only: [:create, :destroy]
  end
  resources :notifications,       only: :index
  resources :password_edits,      only: [:edit, :update]
end
