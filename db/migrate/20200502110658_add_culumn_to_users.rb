class AddCulumnToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :full_name, :string
    add_column :users, :web_site, :string
    add_column :users, :introduction, :string
    add_column :users, :phone_number, :string
    add_column :users, :gender, :integer
  end
end
